# Galactic Escape

Galactic Escape is a fun and challenging space-themed game built using Pygame. Dodge the stars while navigating through space and try to survive as long as possible. Keep track of your time and lives, and see how you rank on the leaderboard.

## Table of Contents

- [Requirements](#requirements)
- [Installation](#installation)
- [Usage](#usage)
- [Controls](#controls)
- [Leaderboard](#leaderboard)
- [Contributing](#contributing)

## Requirements

- Python 3.x
- Pygame

## Installation

1. Clone the repository:

```bash
git clone https://gitlab.com/Jeffery-Hebert/GalacticEscape
```

2. Install the required dependencies:

```bash
pip install -r requirements.txt
```

### Usage

To run the game, simply execute the following command in your terminal:

```python
python main.py
```

## Controls

- Move left: Left Arrow Key
- Move right: Right Arrow Key

## Leaderboard

The game records your elapsed time and saves it to a leaderboard in a CSV file named `leaderboard.csv`. Compare your scores and challenge your friends!

## Contributing

If you'd like to contribute to Galactic Escape, please feel free to fork the repository and submit a pull request. For any bug reports or feature requests, please open an issue on the repository's issue tracker.
